# To Play

> python play.py

Tile-based game with procedural/randomly generated tiles, coded in Python using PyGame.

Collect as many coins as you can, or escape before the flying skulls and eyes get you.

![Screenshot](http://i.imgur.com/02V4vYl.png)