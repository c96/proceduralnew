import os, sys
import pygame
import random

from pygame.locals import *
if not pygame.display: print 'Warning, display disabled'
if not pygame.font: print 'Warning, fonts disabled'

from gameutils import *
from gametypes import *
from gamelevels import *

BLOCK_SIZE = 32

# GAME CONTROLLER MAIN

class Game:

    def __init__(self, width=640,height=480):

        self.width = width
        self.height = height
        self.screen = pygame.display.set_mode((self.width, self.height))

        self.block_size = BLOCK_SIZE

        self.total_score = 0
        self.high_score = 0

        # Set up the display
        pygame.display.set_caption("Game")

    def prepare_level(self):

        # Update sprite groups for render updates
        self.player_sprites = pygame.sprite.RenderUpdates()
        self.enemy_sprites = pygame.sprite.RenderUpdates()
        self.block_sprites = pygame.sprite.RenderUpdates()
        self.coin_sprites = pygame.sprite.RenderUpdates()
        self.exit_sprites = pygame.sprite.RenderUpdates()
        self.static_sprites = pygame.sprite.RenderUpdates()

        self.player_sprites.empty()
        self.enemy_sprites.empty()
        self.block_sprites.empty()
        self.coin_sprites.empty()
        self.exit_sprites.empty()
        self.static_sprites.empty()

        self.clock = pygame.time.Clock()

        self.player = None

        self.load_level()
        self.load_sprites()

        # background
        self.background = pygame.image.load(os.path.join('data', 'images', 'background.png')).convert()
        self.background = pygame.transform.scale(self.background, (self.width, self.height))  

        self.static_sprites.draw(self.screen)
        self.static_sprites.draw(self.background)

    def load_level(self):

        # Offeset for sprites
        self.x_offset = (BLOCK_SIZE/2)
        self.y_offset = (BLOCK_SIZE/2)

        # Level generator
        self.level = GameLevelRandom()

        # Generate a new level width/height
        self.level.generate_level(self.level.tile_size,self.level.tile_size)

        # Get assetMap for rendering
        self.layout = self.level.get_layout()

    def load_sprites(self):

        # Load in sprites image list to draw images in tiles
        self.img_list = self.level.get_sprites()
        
        # Update sprite groups for render updates
        self.player_sprites = pygame.sprite.RenderUpdates()
        self.enemy_sprites = pygame.sprite.RenderUpdates()
        self.block_sprites = pygame.sprite.RenderUpdates()
        self.coin_sprites = pygame.sprite.RenderUpdates()
        self.exit_sprites = pygame.sprite.RenderUpdates()
        self.static_sprites = pygame.sprite.RenderUpdates()

        #print 'layout'
        #print self.layout

        # Create all sprites in sprite groups
        for y in xrange(len(self.layout)):
            for x in xrange(len(self.layout[y])):
                # center point
                centerPoint = [(x*BLOCK_SIZE)+self.x_offset,(y*BLOCK_SIZE+self.y_offset)]

                if self.layout[x][y]==self.level.BLOCK:
                    block = Sprite(centerPoint, self.img_list[self.level.BLOCK])
                    self.block_sprites.add(block)

                elif self.layout[x][y]==self.level.PLAYER:
                    self.player = Player(self, centerPoint,self.img_list[self.level.PLAYER])
                    self.player_sprites.add(self.player)

                elif self.layout[x][y]==self.level.COIN:
                    coin = Sprite(centerPoint, self.img_list[self.level.COIN])
                    self.coin_sprites.add(coin) 

                elif self.layout[x][y]==self.level.TILED:
                    tiled = Sprite(centerPoint, self.img_list[self.level.TILED])
                    self.static_sprites.add(tiled) 

                elif self.layout[x][y]==self.level.BRKNTILE:
                    brkntile = Sprite(centerPoint, self.img_list[self.level.BRKNTILE])
                    self.static_sprites.add(brkntile) 

                elif self.layout[x][y]==self.level.RUINTILE:
                    ruintile = Sprite(centerPoint, self.img_list[self.level.RUINTILE])
                    self.static_sprites.add(ruintile) 

                elif self.layout[x][y]==self.level.DIRTTILE:
                    dirttile = Sprite(centerPoint, self.img_list[self.level.DIRTTILE])
                    self.static_sprites.add(dirttile) 

                elif self.layout[x][y]==self.level.EXIT:
                    exit = Sprite(centerPoint, self.img_list[self.level.EXIT])
                    self.exit_sprites.add(exit) 

                elif self.layout[x][y]==self.level.ENEMYEYE:
                    enemyeye = Enemy(self, "enemyeye", centerPoint, self.img_list[self.level.ENEMYEYE])
                    enemyeye.speed = .5
                    self.enemy_sprites.add(enemyeye) 

                elif self.layout[x][y]==self.level.ENEMYSKULL:
                    enemyskull = EnemyPathfinder(self, "enemyskull", centerPoint, self.img_list[self.level.ENEMYSKULL])
                    enemyskull.speed = .8
                    self.enemy_sprites.add(enemyskull) 

    def pause(self):
        self.running = False

    def resume(self):
        self.running = True

    def regen(self):
        self.pause()
        self.handle_stats()
        pygame.time.delay(20)
        self.run()

    def run(self):
        
        self.prepare_level()

        self.running = True

        pygame.display.flip()

        while self.running:
            
            self.clock.tick(60)
            
            # exit game
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False
                    sys.exit()

            # Move the player if an arrow key is pressed
            key = pygame.key.get_pressed()
            if key[pygame.K_LEFT]:
                self.player.move(-2, 0)
            if key[pygame.K_RIGHT]:
                self.player.move(2, 0)
            if key[pygame.K_UP]:
                self.player.move(0, -2)
            if key[pygame.K_DOWN]:
                self.player.move(0, 2)

            # regen on 'r'
            if key[pygame.K_r]:
                self.regen()
                pygame.time.delay(20)
            # quit
            elif key[pygame.K_ESCAPE]:
                self.running = False                    
                sys.exit()

            # Draw the scene
            self.screen.fill((0, 0, 0))

            self.static_sprites.clear(self.screen,self.background)
            self.screen.blit(self.background, (0, 0))  

            # Check player collisions and update on instances        
            self.player_sprites.update()      
            self.enemy_sprites.update()

            # Prepare sprite list for rendering in groups
            reclist = self.static_sprites.draw(self.screen)
            reclist += self.exit_sprites.draw(self.screen)
            reclist += self.block_sprites.draw(self.screen)
            reclist += self.coin_sprites.draw(self.screen)
            reclist += self.enemy_sprites.draw(self.screen)
            reclist += self.player_sprites.draw(self.screen)

            reclist = self.render_hud(reclist) 

            pygame.display.update(reclist)

    def add_score(self, increment):
        self.total_score += increment

    def handle_stats(self):
        if self.player:            
            if self.player.coins > self.high_score:
                self.high_score = self.player.coins

    def render_hud(self, reclist):
        if self.player:

            if pygame.font:

                # centerx=(self.background.get_width()/2) + 100, centery=(10)

                reclist = self.render_text_item(reclist, "CoinBot", (58, 15), 36)

                reclist = self.render_text_item(reclist, "Health: %s" % self.player.health, (180, 18), 24)
                reclist = self.render_text_item(reclist, "Score: %s" % self.player.coins, (280, 18), 24)


                reclist = self.render_text_item(reclist, "Total Score: %s" % self.total_score, (400, 18), 24)
                reclist = self.render_text_item(reclist, "High Score: %s" % self.high_score, (540, 18), 24)

                reclist = self.render_text_item(reclist, "'R' to replay/regen or find an exit...", (self.background.get_width()/2, self.background.get_height() - 10), 18)

        return reclist

    def render_text_item(self, reclist, val, rect, font_size = 24, color = (255, 255, 255)):

        if pygame.font:

            font = pygame.font.Font(None, font_size)
            text = font.render(val, 1, color)
            text_rect =  text.get_rect(centerx=(rect[0]), centery=(rect[1]))

            self.screen.blit(text, text_rect)

            reclist += [text_rect]  

        return reclist
                    
if __name__ == "__main__":

    pygame.init()

    game = Game(672,672)
    game.run()