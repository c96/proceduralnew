import os
import pygame
import random
from pygame.locals import *

from gametypes import *
from gameutils import *

BLOCK_SIZE = 32
 
class GameLevelRandom(object):
    
    def __init__(self):

        self.COIN = 0

        self.BLOCK = 1
        self.PLAYER = 2
        self.TILED = 3
        self.BRKNTILE = 4
        self.RUINTILE = 5
        self.DIRTTILE = 6

        self.EXIT = 7

        self.ENEMYEYE = 8
        self.ENEMYSKULL = 9

        self.tile_size = 21
        self.width = self.tile_size
        self.height = self.tile_size

    def generate_level(self, width, height):

        self.width = width
        self.height = height

        self.generate_layout(width, height)

    def generate_layout(self, width, height):

        # maps to track current level layout and player/block placement for pathfinding
        # asset map / level layout
        self.assetMap =  [[0 for x in range(width)] for y in range(height)]

        enemy_added = False
        
        for x in range (0,width):
            for y in range(0, height):
                if y == 0 or x == 0 or y == width - 1 or x == height - 1:
                    self.assetMap[x][y] = self.BLOCK
                elif y == 1 and x == 1:
                    self.assetMap[x][y] = self.PLAYER                   
                elif (y == 1 and x == 2) or (x == 1 and y == 2):
                    # Don't load right around player
                    pass
                else:
                    chance_enemy = random.randint(0, 50)

                    if chance_enemy == 50:
                        if enemy_added == False:
                            enemy_added = True
                            #self.assetMap[x][y] = 9 # testing astar with one
                        #self.assetMap[x][y] = 9 # test only astar
                        #self.assetMap[x][y] = 0 # test no enemies
                        self.assetMap[x][y] = random.randint(8,9) # normal enemies, comment if testing astar
                    else:                           
                        chance = random.randint(0,1)
                        if chance == 1:

                            chance_exit = random.randint(0, 60)
                            if chance_exit == 60:
                                self.assetMap[x][y] = self.EXIT
                            else:
                                self.assetMap[x][y] = random.randint(3,6)
                        else:
                            self.assetMap[x][y] = random.randint(0,1)
        self.nodes = []

        for x in range (0,height):
            for y in range(0, width):     
                if self.assetMap[x][y] == self.BLOCK:
                    self.nodes.append((x, y))

    def get_nodes(self):
        return self.nodes
    
    def get_layout(self):
        return self.assetMap;

    def get_sprites(self):
        coin, rect = load_image('coin.png',-1)
        block, rect = load_image('block.png')
        player, rect = load_image('player.png',-1)
        tiled, rect = load_image('tiletile.png',-1)
        brkntile, rect = load_image('brokentile.png',-1)
        ruintile, rect = load_image('ruinedtile.png',-1)
        dirttile, rect = load_image('dirttile.png',-1)
        exit, rect = load_image('exit.png',-1)
        enemyeye, rect = load_image('enemyeye.png',-1)
        enemyskull, rect = load_image('enemyskull.png',-1)
        return [coin,block,player,tiled,brkntile,ruintile,dirttile,exit,enemyeye,enemyskull]

    # -------------------------------------------------------------
    # PATHFINDING HELPERS

    def get_grid_tile(self, posX, posY):
        return [int((posX / BLOCK_SIZE)), int((posY / BLOCK_SIZE))]

    def get_grid_tile_pos(self, tileX, tileY):
        return [int(tileX * BLOCK_SIZE), int(tileY * BLOCK_SIZE)]
