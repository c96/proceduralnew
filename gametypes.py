#! /usr/bin/env python

import random

import pygame

from pygame.locals import *

from gameutils import *

# GENERIC, NON ACTIVE SPRITES i.e. blocks + coins
class Sprite(pygame.sprite.Sprite):
        
    def __init__(self, centerPoint, image):
        pygame.sprite.Sprite.__init__(self) 
        self.image = image
        self.rect = image.get_rect()
        self.rect.center = centerPoint
        
# BASE GAME SPRITE WITH ASSET LOADING
class GameSprite(pygame.sprite.Sprite):
        
    def __init__(self, type, centerPoint, image = None):
        pygame.sprite.Sprite.__init__(self) 
        
        self.load(type, centerPoint, image)

    def load(self, type, centerPoint, image):

        if image == None:
            self.image, self.rect = load_image(type + '.png',-1)
        else:
            self.image = image
            self.rect = image.get_rect()
        
        self.rect.center = centerPoint

# BASE GAME PLAYER SPRITE WITH MOVEMENT
class GamePlayer(GameSprite):
            
    def __init__(self, game, type, centerPoint, image = None):
        GameSprite.__init__(self, type, centerPoint, image) 

        self.game = game

        self.speed = 1
        self.health = 9

        self.x_dist = 2
        self.y_dist = 2 
        self.xMove = 0
        self.yMove = 0

        self.last_hit_time = 0

    def hit(self, power):

        # If time has passed since last hit, allow hit could be integrated with player state/speed
        if self.last_hit_time + random.randint(60,120) <= pygame.time.get_ticks()  :
            self.last_hit_time = pygame.time.get_ticks()  
            self.health -= power
        
        return self.is_alive()

    def is_alive(self):

        if self.health <= 0:
            return True
        else:
            return False  

    def handle_regen_game(self):
        should_regen = (self.is_alive() == False)
        if should_regen:
            print ' HEALTH - Regenerating map'
            self.regen_game()

    def regen_game(self):
        self.game.regen() 
        
    def move(self, dx, dy):
        
        if dx != 0:
            self.move_single_axis(dx, 0, True)
        if dy != 0:
            self.move_single_axis(0, dy, True)

    def fly(self, dx, dy):
        
        if dx != 0:
            self.move_single_axis(dx, 0, False)
        if dy != 0:
            self.move_single_axis(0, dy, False)
    
    def move_single_axis(self, dx, dy, check_collisions):
        
        # Move the rect
        self.rect.x += dx * self.speed
        self.rect.y += dy * self.speed

        if check_collisions:
            # If you collide with a wall, move out based on velocity
            for wall in self.game.block_sprites:
                if self.rect.colliderect(wall.rect):
                    if dx > 0: # Moving right; Hit the left side of the wall
                        self.rect.right = wall.rect.left
                    if dx < 0: # Moving left; Hit the right side of the wall
                        self.rect.left = wall.rect.right
                    if dy > 0: # Moving down; Hit the top side of the wall
                        self.rect.bottom = wall.rect.top
                    if dy < 0: # Moving up; Hit the bottom side of the wall
                        self.rect.top = wall.rect.bottom

# MAIN PLAYER CHARACTER
class Player(GamePlayer):
            
    def __init__(self, game, centerPoint, image = None):
        GamePlayer.__init__(self, game, 'player', centerPoint, image) 
    
        self.coins = 0
        self.health = 9
        self.speed = 2

    def handle_collision(self):
        lstCols = pygame.sprite.spritecollide(self, self.game.coin_sprites, True, pygame.sprite.collide_rect_ratio(.7))
        if (len(lstCols)>0):
            increment = len(lstCols)
            self.coins += increment
            self.game.add_score(increment)

        lstCols = pygame.sprite.spritecollide(self, self.game.enemy_sprites, False, pygame.sprite.collide_rect_ratio(.7))
        if (len(lstCols)>0):
            should_regen = self.hit(len(lstCols))
            if should_regen:
                print ' HEALTH - Regenerating map'
                self.game.regen()

        lstCols = pygame.sprite.spritecollide(self, self.game.exit_sprites, False, pygame.sprite.collide_rect_ratio(.7))
        if (len(lstCols)>0):
           print ' EXIT - Regenerating map'
           self.regen_game()

    def update(self):

        if(self.game.running == False):
            return

        self.handle_collision()
                      

# ENEMY TYPE THAT GOES THROUGH WALLS
class Enemy(GamePlayer):
            
    def __init__(self, game, type, centerPoint, image = None):
        GamePlayer.__init__(self, game, type, centerPoint, image) 

        self.health = 3

    def handle_movement(self):

        if self.game.player.rect.x > self.rect.x:
            self.xMove = self.x_dist * self.speed
        elif self.game.player.rect.x < self.rect.x:
            self.xMove = -self.x_dist * self.speed

        if self.game.player.rect.y > self.rect.y:
            self.yMove = self.y_dist * self.speed
        elif self.game.player.rect.y < self.rect.y:
            self.yMove = -self.y_dist * self.speed
        
        if (self.xMove==0)and(self.yMove==0):
            return

        # Using move_ip to move over walls, move is for within walls
        self.rect.move_ip(self.xMove,self.yMove)


    def handle_collision(self):                
        lstCols = pygame.sprite.spritecollide(self, self.game.block_sprites, False, pygame.sprite.collide_rect_ratio(.6))
        
        # Using move_ip to move over walls, move is for within walls
        # Move over walls faster

        if (len(lstCols)>0):
            if self.xMove > 0:
                self.fly(-self.xMove, self.yMove)
            if self.xMove < 0:
                self.fly(self.xMove, self.yMove)
            if self.yMove > 0:
                self.fly(self.xMove, self.yMove)
            if self.yMove > 0:
                self.fly(self.xMove, -self.yMove)

        # Enemy to player collisions, all regen in player class for single instance
        #lstCols = pygame.sprite.spritecollide(self, self.game.player_sprites, False, pygame.sprite.collide_rect_ratio(.6))
        #if (len(lstCols)>0):
            # hit the player
            #print ' HIT - Player'            
            #should_regen = self.game.player.hit(len(lstCols))
            #if should_regen:
                #print ' HEALTH - Regenerating map'
                #self.game.regen()  

    def update(self):

        if(self.game.running == False):
            return

        self.handle_movement()

        self.handle_collision()


# ENEMY TYPE THAT PATHFINDS USING A*
class EnemyPathfinder(GamePlayer):
            
    def __init__(self, game, type, centerPoint, image = None):
        GamePlayer.__init__(self, game, type, centerPoint, image) 

        self.health = 3

        # Current enemy tile position
        self.next_pos = (1, 1)
        self.current_pos = (self.rect.x, self.rect.y)
        
        # Current main player tile position
        self.last_tile_pos_player = [0,0]
        self.tile_pos_player = [1,1]

        # Pathfinding support
        self.astar = AStar()
        self.path = None     
        self.preparing = False
        self.traversing = False

        self.last_pathfind_time = random.randint(30,500)

    def generate_graph(self):
        self.astar.generate_graph(self.game.level.nodes, self.game.level.tile_size, self.game.level.tile_size)

    def update_graph(self):

        self.preparing = True

        self.generate_graph()

        self.tile_pos = self.game.level.get_grid_tile(self.rect.x, self.rect.y)
        self.tile_pos_player = self.game.level.get_grid_tile(self.game.player.rect.x, self.game.player.rect.y)
        self.target_pos = self.tile_pos_player

        #print 'tile_pos'
        #print self.tile_pos

        #print 'tile_pos_player'
        #print self.tile_pos_player

        #try:
        self.path = self.astar.get_path(self.tile_pos, self.tile_pos_player)

        if self.path:

            self.path_length = len(self.path)

            self.current_node_index = self.path_length - 1

            #print 'path'
            #print self.path

        #except Exception as e:
            # accessing nodes error
            #print 'ERROR:', e
            #pass
            
        self.preparing = False

    def next_traversing_node(self):

        if self.path:

            if self.path_length > 0 and self.current_node_index >= 0:

                node = self.path[self.current_node_index]

                #print 'node'
                #print node

                pos = self.game.level.get_grid_tile_pos(node[0], node[1])

                self.target_pos = [node[0], node[1]]

                self.next_pos = pos

                #print 'next_pos'
                #print self.next_pos
                
                xTo = pos[0]
                yTo = pos[1]

                #print 'xTo'
                #print xTo

                #print 'yTo'
                #print yTo

                adjustmove = False

                if(xTo != self.rect.x):
                    adjustmove = True
                    if(xTo < self.rect.x):
                        self.xMove = -2
                        self.yMove = 0
                    elif(xTo > self.rect.x):
                        self.xMove = 2
                        self.yMove = 0

                elif(yTo != self.rect.y):
                    adjustmove = True
                    if(yTo < self.rect.y):
                        self.xMove = 0
                        self.yMove = -2
                    elif(yTo > self.rect.y):
                        self.xMove = 0
                        self.yMove = 2

                if adjustmove == False:
                    self.xMove = 0
                    self.yMove = 0
                
                if self.current_node_index < 0:

                    self.traversing = False
            else:
                traversing = False


    def handle_traversing(self):

        if self.preparing:
            return

        self.tile_pos = self.game.level.get_grid_tile(self.rect.x, self.rect.y)
        self.tile_pos_player = self.game.level.get_grid_tile(self.game.player.rect.x, self.game.player.rect.y)
                
        self.current_pos = (self.rect.x, self.rect.y)

        if(self.preparing == False and self.next_pos != self.current_pos or self.advance):
            self.advance = False
            self.traversing = True
            self.next_traversing_node()

    def handle_pathfinding(self):

        if self.preparing:
            return

        self.tile_pos_player = self.game.level.get_grid_tile(self.game.player.rect.x, self.game.player.rect.y)

        # Check if main player has changed tiles, 
        # if so refresh path to the player from the current player
        if self.last_tile_pos_player != self.tile_pos_player:
            self.last_tile_pos_player = self.tile_pos_player
            self.restart_pathfinding()
            #print 'last_tile_pos_player'
            #print self.last_tile_pos_player
            #print 'tile_pos_player'
            #print self.tile_pos_player
        
        # If advance is set previosly in then reset
        self.advance = False
        
        #print 'next_pos'
        #print self.next_pos
        #print 'current_pos'
        #print self.current_pos

        # Figure out the current state based on the current node 
        # and node index in the pathfinding (x, y) sets to navigate
        if(compare_tuple(self.next_pos, self.current_pos)):
            if(self.current_node_index >= 0):
                self.current_node_index = self.current_node_index - 1

                if(self.current_node_index < 0):
                    # If we have cycled through the nodes in reverse
                    #  and there are no more, stop traversing
                    self.traversing = False
                else: 
                    # If we have nodes left in the path and no changes then 
                    # advance to the next node                    
                    self.advance = True

    def restart_pathfinding(self):

        if self.game.running == False:
            return

        self.traversing = False
        self.advance = False
        self.path = None
        self.preparing = False
        self.update_graph()
        #print 'restarting pathfinding'

    def handle_graph(self):        

        # If time has passed update graph, could be integrated with player state/speed
        if self.last_pathfind_time + random.randint(120,500) <= pygame.time.get_ticks():
            self.last_pathfind_time = pygame.time.get_ticks()  
        else:
            return

        # Generate the pathfinding graph
        if(self.preparing == False and self.traversing == False or self.path is None):
            self.update_graph()

    def handle_movement(self):
        # Move the player if there is a move offset
        self.move(self.xMove, self.yMove)

    def handle_collision(self):
        # hit main player
        lstCols = pygame.sprite.spritecollide(self, self.game.player_sprites, False, pygame.sprite.collide_rect_ratio(.6))
        if (len(lstCols)>0):
            # hit the player
            #print ' HIT - Player'            
            should_regen = self.game.player.hit(len(lstCols))
            if should_regen:
                #print ' HEALTH - Regenerating map'
                self.game.regen()  


    def update(self):

        if self.preparing:
            return

        if(self.game.running == False):
            self.traversing = False
            self.preparing = False
            return

        # Handle collision
        self.handle_collision()

        # Handle updating graph whhen it needs to be regenerated
        self.handle_graph()

        # Handle pathfinding states
        self.handle_pathfinding()

        # Handle moving sprite along the path
        self.handle_traversing()

        # Handle movement
        self.handle_movement()
