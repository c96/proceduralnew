import os, sys
import pygame
from pygame.locals import *

import heapq

def compare_tuple(t1, t2): 
    return sorted(t1) == sorted(t2)

def load_image(name, colorkey=None):
    fullname = os.path.join('data', 'images')
    fullname = os.path.join(fullname, name)
    try:
        image = pygame.image.load(fullname)
    except pygame.error, message:
        print 'Cannot load image:', fullname
        raise SystemExit, message
    image = image.convert()
    if colorkey is not None:
        if colorkey is -1:
            colorkey = image.get_at((0,0))
        image.set_colorkey(colorkey, RLEACCEL)
    return image, image.get_rect()


def check_collision(obj1, obj2):
    """
    If the function finds a collision, it will return True;
    if not, it will return False. If one of the objects is
    not the intended type, the function instead returns None.
    """
    try:
        #create attributes
        rect1, mask1, blank1 = obj1.rect, obj1.hitmask, obj1.blank
        rect2, mask2, blank2 = obj2.rect, obj2.hitmask, obj2.blank
        #initial examination
        if rect1.colliderect(rect2) is False:
            return False
    except AttributeError:
        return None
 
    #get the overlapping area
    clip = rect1.clip(rect2)
 
    #find where clip's top-left point is in both rectangles
    x1 = clip.left - rect1.left
    y1 = clip.top  - rect1.top
    x2 = clip.left - rect2.left
    y2 = clip.top  - rect2.top
 
    #cycle through clip's area of the hitmasks
    for x in range(clip.width):
        for y in range(clip.height):
            #returns True if neither pixel is blank
            if mask1[x1+x][y1+y] is not blank1 and \
               mask2[x2+x][y2+y] is not blank2:
                return True
 
    #if there was neither collision nor error
    return False


class Cell(object):
    def __init__(self, x, y, reachable):
        """
        Initialize new cell
        @param x cell x coordinate
        @param y cell y coordinate
        @param reachable is cell reachable? not a wall?
        """
        self.path = []

        self.reachable = reachable
        self.x = x
        self.y = y
        self.parent = None
        self.g = 0
        self.h = 0
        self.f = 0

class AStar(object):
    def __init__(self):
        self.opened = []
        heapq.heapify(self.opened)
        self.closed = set()
        self.cells = []
        self.grid_height = 6
        self.grid_width = 6

    def generate_graph(self, nodes, height, width):
        
        self.closed = None
        self.closed = set()

        self.cells = None
        self.cells = []

        self.grid_height = height
        self.grid_width = width

        for x in range(self.grid_width):
            for y in range(self.grid_height):
                if (x, y) in nodes:
                    reachable = False
                else:
                    reachable = True
                self.cells.append(Cell(x, y, reachable))
        #self.start = self.get_cell(0, 0)
        #self.end = self.get_cell(5, 5)
    
    def get_path(self, start, end):

        #print 'start'
        #print start
        #print 'end'
        #print end

        start = self.get_cell(start[0], start[1])
        end = self.get_cell(end[0], end[1])

        #print 'start2'
        #print start
        #print 'end2'
        #print end

        self.process(start, end)
        
        path = None
        path = []

        cell = end

        if cell is not None:
            path.append((cell.x, cell.y))
            if hasattr(cell, 'parent') and cell.parent is not None:
                while cell.parent is not start and cell.parent is not None:           
                    cell = cell.parent 
                    if hasattr(cell, 'x') and hasattr(cell, 'y'):
                        path.append((cell.x, cell.y))
        return path

    def get_heuristic(self, cell, start, end):
        """
        Compute the heuristic value H for a cell: distance between
        this cell and the ending cell multiply by 10.
        @param cell
        @returns heuristic value H
        """
        return 10 * (abs(cell.x - end.x) + abs(cell.y - end.y))

    def get_cell(self, x, y):
        """
        Returns a cell from the cells list
        @param x cell x coordinate
        @param y cell y coordinate
        @returns cell
        """
        return self.cells[x * self.grid_height + y]

    def get_adjacent_cells(self, cell):
        """
        Returns adjacent cells to a cell. Clockwise starting
        from the one on the right.
        @param cell get adjacent cells for this cell
        @returns adjacent cells list 
        """
        cells = []
        if cell.x < self.grid_width-1:
            cells.append(self.get_cell(cell.x+1, cell.y))
        if cell.y > 0:
            cells.append(self.get_cell(cell.x, cell.y-1))
        if cell.x > 0:
            cells.append(self.get_cell(cell.x-1, cell.y))
        if cell.y < self.grid_height-1:
            cells.append(self.get_cell(cell.x, cell.y+1))
        return cells

    #def display_path(self):
    #    cell = self.end
    #    while cell.parent is not self.start:
    #        cell = cell.parent
    #        print 'path: cell: %d,%d' % (cell.x, cell.y)

    def compare(self, cell1, cell2):
        """
        Compare 2 cells F values
        @param cell1 1st cell
        @param cell2 2nd cell
        @returns -1, 0 or 1 if lower, equal or greater
        """
        if cell1.f < cell2.f:
            return -1
        elif cell1.f > cell2.f:
            return 1
        return 0
    
    def update_cell(self, adj, cell, start, end):
        """
        Update adjacent cell
        @param adj adjacent cell to current cell
        @param cell current cell being processed
        """
        adj.g = cell.g + 10
        adj.h = self.get_heuristic(adj, start, end)
        adj.parent = cell
        adj.f = adj.h + adj.g

    def process(self, start, end):
        # add starting cell to open heap queue
        heapq.heappush(self.opened, (start.f, start))
        while len(self.opened):
            # pop cell from heap queue 
            f, cell = heapq.heappop(self.opened)
            # add cell to closed list so we don't process it twice
            self.closed.add(cell)
            # if ending cell, display found path
            if cell is end:
                #self.process_path(start, end)
                break
            # get adjacent cells for cell
            adj_cells = self.get_adjacent_cells(cell)
            for adj_cell in adj_cells:
                if adj_cell.reachable and adj_cell not in self.closed:
                    if (adj_cell.f, adj_cell) in self.opened:
                        # if adj cell in open list, check if current path is
                        # better than the one previously found
                        # for this adj cell.
                        if adj_cell.g > cell.g + 10:
                            self.update_cell(adj_cell, cell, start, end)
                    else:
                        self.update_cell(adj_cell, cell, start, end)
                        # add adj cell to open list
                        heapq.heappush(self.opened, (adj_cell.f, adj_cell))